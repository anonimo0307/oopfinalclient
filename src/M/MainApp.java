﻿/**
 *@author:Dennis Lee
 *@version:1.0
 *@since:2017-6-25
 */
 package M;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import view.ClientSendToServer;
import view.LoginController;
import view.MenuController;
import view.RegisterDialogController;
import view.RegisterSuccessDialogController;

public class MainApp extends Application{
  /**
    * These are variables of class MainApp.
    */
	private Stage primaryStage;
	private BorderPane rootLayout;
	public static Socket serverSocket;
	PrintWriter clientSendToServer;
	ClientSendToServer clientsendtoserver;
	public LoginController controller;
	String login_consquence;
	public OutputStream outputStream;
	
  /**
    *This method is used to start the process.
    *@param primaryStage This is the parameter of start method.
    *@throws Exception
    */
	@Override
	public void start(Stage primaryStage) throws Exception {
		
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("登入/註冊");

		initRootLayout();
		serverSocket = new Socket("localhost", 8888);
		clientsendtoserver = new ClientSendToServer(serverSocket);
		showLogin();
	}
	/**
    *This method is used to set the RootLayout.
    */
	public void initRootLayout(){
		try{
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("../view/RootLayout.fxml"));
			rootLayout = (BorderPane) loader.load();

			Scene scene = new Scene(rootLayout);
			primaryStage.setScene(scene);
			primaryStage.show();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
  /**
    *This method is used to show the login menu.
    */
	public void showLogin(){
		try{
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("../view/Login.fxml"));
			
			AnchorPane overview = (AnchorPane) loader.load();
			rootLayout.setCenter(overview);

			LoginController	controller = loader.getController();
			controller.setMainApp(this);
			controller.setClientSendToServer(clientsendtoserver);
			this.controller = controller;
			
		}catch(IOException e){
			e.printStackTrace();
		}		
	}
  /**
    *This method is used to show the chat menu.
    */
	public void showMenu(){
		try{
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("../view/Menu.fxml"));
			
			AnchorPane page = (AnchorPane) loader.load();
			Stage MenuStage = new Stage();
			MenuStage.setTitle("登入成功");
			MenuStage.initModality(Modality.WINDOW_MODAL);
			MenuStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			MenuStage.setScene(scene);
			
			MenuController controller = loader.getController();

			controller.setMenuStage(MenuStage);
			controller.setMainApp(this);
			controller.setClientSendToServer(clientsendtoserver);

			MenuStage.showAndWait();
      
		}catch(Exception e){
			
		}
	}
	/**
    *This method is used to show the RegisterDialog menu.
    *@param title This is the first parameter of showRegisterDialog method.
    *@param mainApp This is the second parameter of showRegisterDialog method.
    *@return controller.isOkClicked()
    */
	public boolean showRegisterDialog(String title,MainApp mainApp){
		try{
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("../view/RegisterDialog.fxml"));
			AnchorPane page = (AnchorPane) loader.load();

			Stage dialogStage = new Stage();
			dialogStage.setTitle(title);
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			

			RegisterDialogController controller = loader.getController();
			controller.setDialogStage(dialogStage);
			
			controller.setMainApp(mainApp);

			controller.setClientSendToServer(clientsendtoserver);
			dialogStage.showAndWait();
			return controller.isOkClicked();
		}catch(IOException e){
			System.out.println("show dialog error:");
		
			return false;
		}
	}
	/**
    *This method is used to show the RegisterSuccessDialog menu.
    *@param mainApp This is the parameter of showRegisterSuccessDialog method.
    */
	public void showRegisterSuccessDialog(MainApp mainApp){
		try{
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("../view/RegisterSuccessDialog.fxml"));
			AnchorPane page = (AnchorPane) loader.load();

			Stage dialogStage = new Stage();
			dialogStage.setTitle("");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			
			RegisterSuccessDialogController controller = loader.getController();
			controller.setDialogStage(dialogStage);
			
			controller.setMainApp(mainApp);

			dialogStage.showAndWait();
		
		}catch(IOException e){
			System.out.println("show dialog error:");
		}
	}
	/**
    *This method is used to get the PrimaryStage.
    *@return primaryStage
    */
	public Stage getPrimaryStage(){
		return primaryStage;
	}
  /**
    *This method is used to implement.
    */
	public static void main(String[] args) {
		launch(args);
	}
}