package view;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientSendToServer {
	PrintWriter clientSendToServer;
	
	public ClientSendToServer (Socket seversocket) throws IOException {
		System.out.println(seversocket);
		clientSendToServer = new PrintWriter(seversocket.getOutputStream(), true); 
	}
	
	public void SendMessage (String message){
		clientSendToServer.println(message);
	}
}
