package view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import M.MainApp;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
/**
 * <h1>MenuController</h1>
 * 
 * The dialog's controller
 * <p>
 * 
 * @author Chien-Wei-Chin
 * @version 1.0
 * @since 2017-06-25
 */
public class MenuController extends MainApp {
	private Stage MenuStage;
	private MainApp mainApp;
	private Socket seversocket;

	/**
	 * Getters that gets the socket that connects to the server
	 * 
	 * @return the server socket
	 */
	public Socket getSeversocket() {
		// System.out.println(seversocket);
		return seversocket;
	}

	/**
	 * Setters that sets the server socket
	 * 
	 * @param seversocket
	 *            the server socket to be set
	 */
	public void setSeversocket(Socket seversocket) {

		this.seversocket = seversocket;
		System.out.println("a");
	}

	ClientSendToServer clientsendtoserver;
	@FXML
	private Button btn;
	@FXML
	private Button send;

	/**
	 * Setters that sets the mainApp
	 * 
	 * @param mainApp
	 *            the mainApp to be set
	 */
	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
	}

	/**
	 * Setters that sets the Stage
	 * 
	 * @param MenuStage
	 *            the stage to be set
	 */
	public void setMenuStage(Stage MenuStage) {
		this.MenuStage = MenuStage;
	}

	String globalValue;

	/**
	 * Setters that sets the socket for sending message to server
	 * @param clientsendtoserver
	 * 			the client to server socket to be set
	 */
	public void setClientSendToServer(ClientSendToServer clientsendtoserver) {
		this.clientsendtoserver = clientsendtoserver;
	}

	@FXML
	private TextArea dialog;

	@FXML
	private TextField InputMessage;
	/**
	 * handles the users input for sending message
	 */
	@FXML
	public void handleInput()  {

		String message;
		message = InputMessage.getText();
		clientsendtoserver.SendMessage(message);
		InputMessage.clear();
	}
	/**
	 * initialize method to set the text area not editable
	 */
	@FXML
	private void initialize()  {

		dialog.setEditable(false);

	}
	/**
	 * Handle the button of start chating
	 */
	@FXML
	public void startChat() {
		InputMessage.setVisible(true);

		send.setVisible(true);
		btn.setVisible(false);
		Thread t = new Thread(new ServerReader());
		t.start();

	}
	/**
	 * Handle the exit button and close the program
	 */
	@FXML
	public void handleExit() {
		Platform.exit();
		System.exit(0);
	}
	/**
	 * <h1>ServerReader</h1>
	 * 
	 * The Thread class to read message from server
	 * <p>
	 * 
	 * @author Chien-Wei-Chin
	 * @version 1.0
	 * @since 2017-06-25
	 */
	public class ServerReader implements Runnable {
		/**
		 * Starting method for the thread,gets the message
		 * and show it on the text area
		 */
		@Override
		public void run() {

			try {
				System.out.println(M.MainApp.serverSocket);
				BufferedReader inputReader = new BufferedReader(new InputStreamReader(serverSocket.getInputStream()));

				String lineBuffer;
				String dia = "";
				while ((lineBuffer = inputReader.readLine()) != null) {
					dia = dia + lineBuffer + "\n";
					dialog.setText(dia);
					dialog.positionCaret(dia.length());
				}

				inputReader.close();

			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}
}
