package view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import M.MainApp;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class RegisterDialogController extends MainApp{
	private Socket serverSocket;
	private Stage dialogStage;
	private MainApp mainApp;
	private String key;
	PrintWriter clientSendToServer;
	ClientSendToServer clientsendtoserver;
	
	@FXML
	private TextField account;

	@FXML
	private PasswordField password1;
	@FXML
	private PasswordField password2;

	@FXML
	private Label l1;
	
	public RegisterDialogController() throws UnknownHostException, IOException{
//		serverSocket = new Socket("localhost", 8888);
	     
	      // Start a thread to read output from the server.
//	    serverReader = new StreamSwapper(serverSocket.getInputStream(), System.out);
//	    serverReader.start();
	}
	
	public void setMainApp(MainApp mainApp){
		this.mainApp = mainApp;
	}
	public void setSocket(Socket seversocket) {
		this.serverSocket = seversocket;
	}
	public void setPrintWriter(PrintWriter clientSendToServer) {
		this.clientSendToServer = clientSendToServer;
	}
	public void setClientSendToServer (ClientSendToServer clientsendtoserver){
		this.clientsendtoserver = clientsendtoserver;
	}
	private boolean okClicked = false;

	public void setDialogStage(Stage dialogStage){
		this.dialogStage = dialogStage;
	}
	public boolean isOkClicked(){
		return okClicked;
	}
	private boolean isPasswordValid(){
		if (password1.getText().equals(password2.getText())) {
			return true;
		}else{
			return false;
		}
	}
	private boolean isAccountValid()  throws UnknownHostException, IOException {
		
		// Start a thread to read output from the server.
	    String Account;
	    String Password;
	    clientsendtoserver.SendMessage("register");
	    Account = account.getText();
	    clientsendtoserver.SendMessage(Account);
	    Password = password1.getText();
	    clientsendtoserver.SendMessage(Password);
	    BufferedReader inputReader = new BufferedReader(new InputStreamReader(M.MainApp.serverSocket.getInputStream()));
		String temp;
		temp = inputReader.readLine();
		inputReader = null;
		
		if (temp.equals("establish")){
			return true;
		}	
		else{
			return false;
		}
	}

	@FXML
	private void handleOk() throws IOException{
		boolean isEmpty =    password1.getText().equals("") 
						  || password2.getText().equals("")
					      || account.getText().equals("");
		String pattern = "^[a-zA-Z1-9]*$";
		boolean isAlphatCorrect = password1.getText().matches(pattern) && account.getText().matches(pattern);
		boolean isPV = isPasswordValid();
		boolean isLengthCorrect =  !((password1.getText().length() > 10) || (account.getText().length() > 10));
		if ( isPV && (!isEmpty) && isLengthCorrect && isAlphatCorrect ){ 
			if (isAccountValid()) {
				mainApp.showRegisterSuccessDialog(mainApp);
				okClicked = true;
//				dialogStage.close();
				
			}else {
				l1.setText("此帳號可能已被註冊，請設定另一組帳號");
			}

		}else if (!isAlphatCorrect){
			l1.setText("使用不合法字元！");
		}
		else if (!isLengthCorrect){
			l1.setText("帳號、密碼長度不得超過10!");
		}
		else if(isEmpty) {
			l1.setText("輸入欄位不得為空!");
		}
		else if (!isPV) {
			l1.setText("兩次密碼輸入不同！");
		}
		else{	
		}
	}
	
	@FXML
	private void handleCancel(){
		dialogStage.close();
	}


}
