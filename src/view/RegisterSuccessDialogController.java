package view;

import M.MainApp;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class RegisterSuccessDialogController {
	private Stage dialogStage;
	private MainApp mainApp;
	

	

	public void setMainApp(MainApp mainApp){
		this.mainApp = mainApp;
	}

	public void setDialogStage(Stage dialogStage){
		this.dialogStage = dialogStage;
	}
	
	@FXML
	private void handleContinue(){
		dialogStage.close();
	}

}
