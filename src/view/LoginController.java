package view;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import javax.swing.text.View;

//import javax.swing.text.View;

import M.MainApp;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.stage.Stage;

public class LoginController extends MainApp {
	String lineBuffer;
	private Stage dialogStage;
	private Stage MenuStage;
	private MainApp mainApp;
	public String Account;
	public String Password;
	public String Content;
	
	PrintWriter clientSendToServer;
	ClientSendToServer clientsendtoserver;
	//public StreamSwapper serverReader;
	//private InputStream inputStream;
	
	
	public LoginController() throws IOException {
	}

	public void setMenuStage(Stage MenuStage) {
		this.MenuStage = MenuStage;
	}

	private boolean okClicked = false;

	public boolean isOkClicked(){
		return okClicked;
	}
	public String getAccount() {
		return Account;
	}

	public void setPrintWriter(PrintWriter clientSendToServer) {
		this.clientSendToServer = clientSendToServer;
	}
	
	public void setClientSendToServer (ClientSendToServer clientsendtoserver){
		this.clientsendtoserver = clientsendtoserver;
	}
	@FXML
	private Label loginerror;
	@FXML
	private TextField account;
	@FXML
	private TextField password;

	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
	}

	@FXML
	private void handleLogin() throws UnknownHostException, IOException {
		clientsendtoserver.SendMessage("login");
		Account = account.getText();
		clientsendtoserver.SendMessage(Account);
		Password = password.getText();
		clientsendtoserver.SendMessage(Password);
		if (isLoginSuccess()) {
//			Thread t = new Thread(new ServerReader());
//		    t.start();
			mainApp.showMenu();
			
		} else {
			loginerror.setText("帳號密碼錯誤！");
		}
	}

	@FXML
	private void handleRegister() throws IOException {
		boolean okClicked = mainApp.showRegisterDialog("註冊", mainApp);
		if (okClicked) {

		}

	}

	private boolean isLoginSuccess() throws UnknownHostException, IOException {
		// to do
		// Start a thread to read output from the server.
		
		BufferedReader inputReader = new BufferedReader(new InputStreamReader(serverSocket.getInputStream()));
		String temp;
		temp = inputReader.readLine();
		inputReader = null;
		if (temp.equals("access"))
			return true;
		else
			return false;
	}

//	public TextArea getDialog() {
//		return dialog;
//	}
//
//	public void setDialog(TextArea dialog) {
//		this.dialog = dialog;
//	}
}
